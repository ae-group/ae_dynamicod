<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# dynamicod 0.3.15

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_dynamicod/develop?logo=python)](
    https://gitlab.com/ae-group/ae_dynamicod)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_dynamicod/release0.3.14?logo=python)](
    https://gitlab.com/ae-group/ae_dynamicod/-/tree/release0.3.14)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_dynamicod)](
    https://pypi.org/project/ae-dynamicod/#history)

>ae_dynamicod module 0.3.15.

[![Coverage](https://ae-group.gitlab.io/ae_dynamicod/coverage.svg)](
    https://ae-group.gitlab.io/ae_dynamicod/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_dynamicod/mypy.svg)](
    https://ae-group.gitlab.io/ae_dynamicod/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_dynamicod/pylint.svg)](
    https://ae-group.gitlab.io/ae_dynamicod/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_dynamicod)](
    https://gitlab.com/ae-group/ae_dynamicod/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_dynamicod)](
    https://gitlab.com/ae-group/ae_dynamicod/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_dynamicod)](
    https://gitlab.com/ae-group/ae_dynamicod/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_dynamicod)](
    https://pypi.org/project/ae-dynamicod/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_dynamicod)](
    https://gitlab.com/ae-group/ae_dynamicod/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_dynamicod)](
    https://libraries.io/pypi/ae-dynamicod)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_dynamicod)](
    https://pypi.org/project/ae-dynamicod/#files)


## installation


execute the following command to install the
ae.dynamicod module
in the currently active virtual environment:
 
```shell script
pip install ae-dynamicod
```

if you want to contribute to this portion then first fork
[the ae_dynamicod repository at GitLab](
https://gitlab.com/ae-group/ae_dynamicod "ae.dynamicod code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_dynamicod):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_dynamicod/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.dynamicod.html
"ae_dynamicod documentation").
